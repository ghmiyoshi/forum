package br.com.alura.forum.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.alura.forum.modelo.Curso;

@RunWith(SpringRunner.class)
@DataJpaTest
//@AutoConfigureTestDatabase(replace = Replace.NONE) Para não substituir as configurações do banco usado para o h2
@ActiveProfiles("test") // Quando executar essa classe de teste, forço o profile ativo test
public class ICursoRepositoryTest {

	@Autowired
	private ICursoRepository cursoRepository;
	
	@Autowired
	private TestEntityManager em;

	@Test
	public void deveCarregarUmCursoAoBuscarPeloNome() {
		String nomeCurso = "HTML 5";
		
		Curso html = new Curso();
		html.setNome(nomeCurso);
		html.setCategoria("Programação");
		
		em.persist(html);
		
		Curso curso = cursoRepository.findByNome(nomeCurso);

		assertNotNull(curso);
		assertEquals("HTML 5", curso.getNome());
	}
	
	@Test
	public void NaodeveCarregarUmCursoCujoNomeNaoEstaCadastrado() {
		Curso curso = cursoRepository.findByNome("JPA");

		assertNull(curso);
	}

}
