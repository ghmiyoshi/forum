package br.com.alura.forum.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.alura.forum.controller.dto.DetalhesDoTopicoDto;
import br.com.alura.forum.controller.dto.TopicoDto;
import br.com.alura.forum.controller.form.AtualizacaoTopicoForm;
import br.com.alura.forum.controller.form.TopicoForm;
import br.com.alura.forum.modelo.Topico;
import br.com.alura.forum.repository.ICursoRepository;
import br.com.alura.forum.repository.ITopicoRepository;

@RestController
@RequestMapping("/topicos")
public class TopicosController {

	@Autowired
	private ITopicoRepository topicoRepository;
	
	@Autowired
	private ICursoRepository cursoRepository;

	@GetMapping
	@Cacheable(value = "listaDeTopicos") // Indico ao Spring o nome do cache associado a um determinado método
	public Page<TopicoDto> listar(@RequestParam(required = false) String nomeCurso, @PageableDefault(sort = "id", direction = Direction.DESC, page = 0, size = 10) Pageable paginacao) { // Uso DTO para devolver para o cliente somente os campos que eu quero  
		/* @RequestParam diz para o Spring que é um parâmetro de url e obrigatório mas posso indicar se é opcional usando o required 
		   O objeto Page, além de devolver os registros, o Spring também devolve informações sobre a paginação no JSON de resposta, como número total de registros e páginas
		
		   @PageableDefault indico como será a ordenação default quando não envio o parâmetro sort
		   Uso o Pageable do Spring para simplificar a paginação e ordenação. O objeto recebe a página, quantidade e ordenação só preciso habilitar na classe main (ForumApplication)
		 */
		System.out.println(nomeCurso);
		
		if (nomeCurso == null) {
			Page<Topico> topicos = topicoRepository.findAll(paginacao);
			return TopicoDto.converter(topicos); // Passo os objetos e me devolve uma lista
		} else {
			Page<Topico> topicos = topicoRepository.findByCursoNome(nomeCurso, paginacao);
			return TopicoDto.converter(topicos);
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true) // Devo usar nos métodos que alteram os registros armazenados em cache pela API
	public ResponseEntity<TopicoDto> cadastrar(@RequestBody @Valid TopicoForm topicoForm, UriComponentsBuilder uriBuilder) { // É como se fosse um DTO, mas a diferença é que o form são dados que chegam do cliente para a API  
		/* @RequestBody diz para o Spring pegar os parâmetros no corpo da requisição e atribuir ao TopicoForm e não na url como parâmetros de url
		   @Valid diz para o Spring executar as validações do Bean Validation 
		*/
		Topico topico = topicoForm.converter(cursoRepository);
		topicoRepository.save(topico);
		
		URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new TopicoDto(topico)); // Uso created para retornar o status HTTP "201 Created" que é boa prática do modelo REST. Esse status significa que a requisição foi processada com sucesso e um novo recurso foi criado no servidor  
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> detalhar(@PathVariable Long id) { // @PathVariable aviso o Spring que esse parâmetro vai vir como parte da url "/topicos/1" e não como parâmetro de url "/topicos?id=1"
		Optional<Topico> topico = topicoRepository.findById(id);
		
		if(topico.isPresent()) {
			return ResponseEntity.ok(new DetalhesDoTopicoDto(topico.get()));
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	@Transactional // Aviso para o Spring commitar a transação no final desse método
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<TopicoDto> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizacaoTopicoForm atualizacaoTopicoForm){ // Uso outro form para separar o que eu cadastro e o que eu atualizo, porque pode ser que eu tenho menos campos que podem ser atualizados pelo cliente 
		Optional<Topico> topico = topicoRepository.findById(id);
		
		if(topico.isPresent()) {
			Topico topicoAtualizado = atualizacaoTopicoForm.atualizar(id, topicoRepository);
			return ResponseEntity.ok(new TopicoDto(topicoAtualizado));
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<?> remover(@PathVariable Long id){ // Uso ? para dizer que é um generics mas não sei o tipo
		Optional<Topico> topico = topicoRepository.findById(id);
		
		if (topico.isPresent()) {
			topicoRepository.deleteById(id);
			return ResponseEntity.ok().build(); // Deixo o ok vazio só para devolver status HTTP "200 OK"
		}
		
		return ResponseEntity.notFound().build(); // Devolvo status HTTP "404 Not Found" para o cliente, evitando que uma exception seja devolvida no corpo da resposta
	}

}
