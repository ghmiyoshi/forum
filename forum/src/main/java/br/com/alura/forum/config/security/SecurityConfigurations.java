package br.com.alura.forum.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.alura.forum.repository.IUsuarioRepository;

@Profile(value = { "prod", "test" })
@EnableWebSecurity // Habilito o uso do Spring Security
@Configuration // Para o Spring carregar e ler as configurações dessa classe
public class SecurityConfigurations extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationService autenticacaoService;
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	@Override
	@Bean // Coloco @Bean para o Spring saber que esse método retorna um AuthenticationManager. Com isso, consigo injetar na AutenticacaoController
	protected AuthenticationManager authenticationManager() throws Exception { // Método que cria um AuthenticationManager
		return super.authenticationManager();
	}

	// Configurações de autenticação
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(autenticacaoService).passwordEncoder(new BCryptPasswordEncoder());  // Digo o encoder que vou utilizar para a senha
	}

	// Configurações de autorização
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests() // Quais requests eu vou autorizar e como será a autorização
		.antMatchers(HttpMethod.GET, "/topicos").permitAll() // Libero "/topicos" para requisições do tipo GET (Lista)
		.antMatchers(HttpMethod.GET, "/topicos/*").permitAll() // Libero "/topicos/ alguma coisa"
		.antMatchers(HttpMethod.POST, "/auth").permitAll() // Libero "/auth" para gerar o token
		.antMatchers(HttpMethod.GET, "/actuator/**").permitAll() // Libero "/actuator" alguma coisa para monitoramento da API
		.antMatchers(HttpMethod.DELETE, "/topicos/*").hasRole("MODERADOR")
		.anyRequest().authenticated() // Indico que preciso estar autenticado para ter acesso a outras URLs que não foram liberadas aqui 
		// .and().formLogin(); Formulário de login tradicional que cria sessão 
		.and().csrf().disable() // Csrf é uma abreviação para cross-site request forgery, que é um tipo de ataque hacker que acontece em aplicações web. 
						 	    // Como vou fazer autenticação via token, automaticamente a API está livre desse tipo de ataque. Por isso, desabilito isso para o Spring security não fazer a validação do token do csrf
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // Quando fazer autenticação, aviso para o Spring Security que não é para criar sessão, porque vou usar token
		.and().addFilterBefore(new AutenticacaoViaTokenFilter(tokenService, usuarioRepository), UsernamePasswordAuthenticationFilter.class); // Antes de fazer autenticação, antes de qualquer coisa, executar o meu filtro para pegar o token
	}
	
	// Configurações de recursos estaticos(js, css, imagens, etc)
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web.ignoring().antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**"); 
	}
	
}
