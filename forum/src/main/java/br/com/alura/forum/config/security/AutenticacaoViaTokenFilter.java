package br.com.alura.forum.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.alura.forum.modelo.Usuario;
import br.com.alura.forum.repository.IUsuarioRepository;

/* Autenticação Stateless 
 * As requisições passam por esse filtro, pega o token e faz todo o processo, ou seja, 
 * a cada requisição o usuário é reautenticado.
 */
public class AutenticacaoViaTokenFilter extends OncePerRequestFilter{ // OncePerRequestFilter é um filtro do Spring que é chamado uma única vez a cada requisição

	private TokenService tokenService; 
	
	private IUsuarioRepository usuarioRepository; // Não consigo injetar porque é um filtro
	
	public AutenticacaoViaTokenFilter(TokenService tokenService, IUsuarioRepository usuarioRepository) {
		this.tokenService = tokenService;
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String token = recuperarToken(request); // Recuperar token
		System.out.println("Token: " + token);
		
		boolean valido = tokenService.isTokenValido(token); // Validar token 
		System.out.println("Token válido: " + valido);
		
		if(valido) {
			autenticarCliente(token);
		}
		
		filterChain.doFilter(request, response); // Digo para seguir o fluxo da requisição
	}

	private void autenticarCliente(String token) {
		Long idUsuario = tokenService.getIdUsuario(token); // Pego o id do token 
		Usuario usuario = usuarioRepository.findById(idUsuario).get(); // Recupero o objeto usuario pelo id 
		
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities()); // Crio o UsernamePasswordAuthenticationToken passando o usuario, senha nulo porque já está autenticado e os seus perfis
		
		SecurityContextHolder.getContext().setAuthentication(authentication); // Chamo a classe do Spring para forçar a autenticação
	}

	private String recuperarToken(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		
		if (token == null || token.isEmpty() || !token.startsWith("Bearer ")) { // nulo/ vazio/ ñ começa com Bearer
			return null;		
		}
		
		return token.substring(7, token.length()); // Pego a partir do 7, ou seja, do espaço até o final da String, no caso, o conteúdo do token
	}  

	
}
