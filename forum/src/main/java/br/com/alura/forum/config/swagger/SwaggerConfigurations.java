package br.com.alura.forum.config.swagger;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.alura.forum.modelo.Usuario;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration // Anotação para o Spring carregar essa classe de configuração
public class SwaggerConfigurations {

	@Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2) // Tipo do documento Swagger 2
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.alura.forum")) // Indico qual o pacote que irá começar a ler as classes do projeto
                .paths(PathSelectors.ant("/**")) // Quais endpoints que o swagger deverá analisar "/ mais qualquer coisa"
                .build() 
                .ignoredParameterTypes(Usuario.class) // Peço para ignorar nos endpoints os parâmetros relacionados à classe Usuario
                .globalOperationParameters( // Adiciono parâmetros globais, ou seja, parâmetros em todos os endpoints
                        Arrays.asList(
                                new ParameterBuilder() // Construo o parâmetro
                                    .name("Authorization") // nome
                                    .description("Header para Token JWT") // descrição
                                    .modelRef(new ModelRef("string")) // tipo
                                    .parameterType("header") // Parâmetro que vai no header da requisição
                                    .required(false) // Parâmetro opcional, porque tem endpoints que não precisam ter 
                                    .build())); // Construindo o objeto
	}

}
