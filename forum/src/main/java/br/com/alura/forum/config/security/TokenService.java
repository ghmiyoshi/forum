package br.com.alura.forum.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.alura.forum.modelo.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {
	
	@Value("${forum.jwt.expiration}") // Pego os valores que estão no arquivo de configuração 'application.properties'
	private String expiration; // 1 dia em milissegundos
	
	@Value("${forum.jwt.secret}") 
	private String secret; // Senha para o algoritmo de encriptação
	
	public String gerarToken(Authentication authentication) {
		Usuario usuarioLogado = (Usuario) authentication.getPrincipal();
		
		Date hoje = new Date();
		Date dataExpiracao = new Date(hoje.getTime() + Long.parseLong(expiration)); // Data de hoje em milisegundos + os milisegundos do expiration, essa será a data de expiração (data atual + 24h)
		
		return Jwts.builder() // Para criar um token
				.setIssuer("API do Fórum da Alura") // Quem é que está gerando esse Token, qual é a aplicação
				.setSubject(usuarioLogado.getId().toString()) // Quem é o usuário, passamos o Id único que identifica quem é esse usuário. Como o id é Long preciso converter para String
				.setIssuedAt(hoje) // Qual a data da geração do Token
				.setExpiration(dataExpiracao) // Qual a data de expiração
				.signWith(SignatureAlgorithm.HS256, secret) // Como iremos criptografar nosso Token (Algoritmo de encriptação + senha)
				.compact(); // Compacta e transforma em uma String e retorna o Token
	}

	public  boolean isTokenValido(String token) {
		try {
			Jwts.parser() // Passo um token para fazer o parser e ele vai descriptografar e verificar se está ok
			.setSigningKey(this.secret) // Nesse parser, primeiro passo a chave secret do application.properties que é usada para criptografar e descriptografar
			.parseClaimsJws(token); // Uso o método parseClaimsJws para recuperar o token e suas informações. Se o token for válido, devolve o objeto, se tiver inválido ou nulo, lança uma exception     
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Long getIdUsuario(String token) { 
		Claims claims = Jwts.parser()
		.setSigningKey(this.secret)
		.parseClaimsJws(token)
		.getBody(); // Esse método devolve o token no body 
		
		return Long.parseLong(claims.getSubject()); // Uso getSubject() para pegar o id do usuario porque quando criei o token usei setSubject(). Converto para long porque tudo do token é String.
	}
}
