package br.com.alura.forum.config.validacao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErroDeValidacaoHandler { 
	
	@Autowired
	private MessageSource messageSource; // classe do Spring que ajuda a pegar mensagens de erro de acordo com o idioma que o cliente requisitar 
	
	/* O handler é como se fosse um interceptador, onde configuro o Spring para sempre que houver um erro, uma exception em algum método de qualquer controller,
	 ele chama automaticamente esse interceptador passando a lista com todos os erros que aconteceram. Em seguida pego essa lista e transformo no meu objeto 
	 ErroDeFormularioDto que só tem o nome do campo e a mensagem para simplificar.
	 O status code padrão a ser devolvido será o 200, mas posso modificá-lo com a anotação @ResponseStatus. */     
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST) //
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErroDeFormularioDto> handler(MethodArgumentNotValidException exception) {
		List<ErroDeFormularioDto> dto = new ArrayList<>();
		List<FieldError> fieldError = exception.getBindingResult().getFieldErrors();
	
		fieldError.forEach(erro -> {
			String mensagem = messageSource.getMessage(erro, LocaleContextHolder.getLocale()); // classe para o Spring descobrir o locale atual do cliente para pegar a mensagem no idioma correto
			
			ErroDeFormularioDto erroDto = new ErroDeFormularioDto(erro.getField(), mensagem);
			
			dto.add(erroDto);
		});
		
		return dto;
	}

}
