package br.com.alura.forum.config.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.alura.forum.modelo.Usuario;
import br.com.alura.forum.repository.IUsuarioRepository;

@Service
public class AuthenticationService implements UserDetailsService { // Indico ao Spring Security que essa é a classe service que executa a lógica de autenticação

	@Autowired
	private IUsuarioRepository usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<Usuario> usuario = usuarioRepository.findByEmail(email);

		if (usuario.isPresent()) { // Encontrei o usuario 
			return usuario.get(); // Retorno esse usuario
		}

		throw new UsernameNotFoundException("Dados inválidos!"); 
	}

}
