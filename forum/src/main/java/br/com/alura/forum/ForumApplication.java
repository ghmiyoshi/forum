package br.com.alura.forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSpringDataWebSupport // habilito esse suporte para o Spring pegar da requisição nos parâmetros da url, os campos, as informações de paginação e ordenação e passar para o Spring Data 
@EnableCaching // habilito o uso de cache
@EnableSwagger2 // habilito o swagger
public class ForumApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForumApplication.class, args);
	}

}
