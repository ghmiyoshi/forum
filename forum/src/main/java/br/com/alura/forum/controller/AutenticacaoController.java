package br.com.alura.forum.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alura.forum.config.security.TokenService;
import br.com.alura.forum.controller.dto.TokenDto;
import br.com.alura.forum.controller.form.LoginForm;

@Profile(value = { "prod", "test" })
@RestController
@RequestMapping("/auth")
public class AutenticacaoController {
	
	@Autowired
	private AuthenticationManager authManager; // O AuthenticationManager dispara o processo de autenticação com usuario e senha
	
	@Autowired
	private TokenService tokenService;

	@PostMapping
	public ResponseEntity<TokenDto> autenticar(@RequestBody @Valid LoginForm form) { // Seguindo o padrão onde os dados que chegam do cliente eu recebo em uma classe Form
		System.out.println(form.getEmail());
		System.out.println(form.getSenha());
		
		UsernamePasswordAuthenticationToken dadosLogin = form.converter();
		
		try {
			Authentication authentication = authManager.authenticate(dadosLogin); // Quando a execução chegar nessa linha o Spring olha as configurações que eu fiz e sabe que é para chamar a classe AutenticacaoService,
																																		// que chama a interface IUsuarioRepository para consultar os dados na base
			String token = tokenService.gerarToken(authentication); // Passo authentication para saber o usuário que está logado no sistema
			System.out.println("Token: " +  token);
			
			return ResponseEntity.ok(new TokenDto(token, "Bearer")); // Se der certo, retorno status "200 OK" e no body o token e o tipo para o cliente
																												 // Bearer é um dos mecanismos de autenticação utilizados no protocolo HTTP
		} catch (AuthenticationException erro) {
			return ResponseEntity.badRequest().build(); // Se der errado, o usuario e senha estiver incorreto, retorno status "400 Bad Request"
		}
	}

}
